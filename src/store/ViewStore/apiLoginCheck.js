export function apiLoginCheck(credentials) {
    console.log("nisulod man guuuuud")
    return fetch(
        // "https://harley.tailpos.com/api/method/tailpos_erpnext.syncPOS.syncData",
        // "http://192.168.88.142:8026/api/method/telecom.attendance_check.login_device",
        "https://erp.echoalerts.com/api/method/telecom.attendance_check.login_device",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                attendanceData: credentials,
            }),
        },
    )
        .then(response => response.json())
        .then(responseJson => {
           return responseJson.message;
        });
}

export function attendanceCheck(credentials) {
    return fetch(
        // // "https://harley.tailpos.com/api/method/tailpos_erpnext.syncPOS.syncData",
        // "http://192.168.88.142:8026/api/method/telecom.attendance_check.attendance_check",
        "https://erp.echoalerts.com/api/method/telecom.attendance_check.attendance_check",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                attendanceData: credentials,
            }),
        },
    )
        .then(response => response.json())
        .then(responseJson => {
            return responseJson.message;
        });
}

export function addTask(details) {
    return fetch(
        // "https://harley.tailpos.com/api/method/tailpos_erpnext.syncPOS.syncData",
        // "http://192.168.88.142:8026/api/method/telecom.attendance_check.add_task",
        "https://erp.echoalerts.com/api/method/telecom.attendance_check.add_task",

        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                attendanceData: details,
            }),
        },
    )
        .then(response => response.json())
        .then(responseJson => {
            return responseJson.message;
        });
}

export function getTask(details) {
    return fetch(
        // "https://harley.tailpos.com/api/method/tailpos_erpnext.syncPOS.syncData",
        // "http://192.168.88.142:8026/api/method/telecom.attendance_check.get_task",
        "https://erp.echoalerts.com/api/method/telecom.attendance_check.get_task",

        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                attendanceData: details,
            }),
        },
    )
        .then(response => response.json())
        .then(responseJson => {
            return responseJson.message;
        });
}

export function timeOut(details) {
    return fetch(
        // "https://harley.tailpos.com/api/method/tailpos_erpnext.syncPOS.syncData",
        // "http://192.168.88.142:8026/api/method/telecom.attendance_check.time_out",
        "https://erp.echoalerts.com/api/method/telecom.attendance_check.time_out",

        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                attendanceData: details,
            }),
        },
    )
        .then(response => response.json())
        .then(responseJson => {
            return responseJson.message;
        });
}
