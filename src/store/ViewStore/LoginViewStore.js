import { types } from "mobx-state-tree";

const LoginInfoStore = types
    .model("LoginInfoStore", {
        user_name: types.optional(types.string, ""),
        password: types.optional(types.string, ""),
        status: types.optional(types.boolean,false)
    })
    .actions(self => ({
        changeUserName(data) {
            self.user_name = data;
        },
        changePassword(data) {
            self.password = data;
        },
        changeStatus(data) {
            self.status = data;
        },
    }));

const LoginInfo = LoginInfoStore.create({});

export default LoginInfo;
