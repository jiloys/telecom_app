import * as React from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right
} from "native-base";
import AddTaskComponent from "../../components/AddTaskComponent"
import GetTaskButtonComponent from "../../components/GetTaskButtonComponent"
import TaskListComponent from "../../components/TaskListComponent"
import styles from "./styles";

class Home extends React.Component {
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Title>Home</Title>
          </Left>
          <Body/>
          <Right />
        </Header>
        <Content>

          <AddTaskComponent
              taskAdd={(data) => {
                this.props.taskAdd(data)
                  // console.log(data)
              }}
              taskName={this.props.taskName}
              onChangeTaskName={(text) => this.props.onChangeTaskName(text)}
          />
          <GetTaskButtonComponent
            getTask={() => this.props.getTask()}
          />
          <TaskListComponent
            taskList={this.props.tasksList}
            timeOutTask={(item) => this.props.timeOutTask(item)}
          />
        </Content>
      </Container>
    );
  }
}

export default Home;
