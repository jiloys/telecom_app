import * as React from "react";
import { Image, Dimensions } from "react-native";
import {
  Container,
  Content,
  Header,
  Body,
  Title,
  Button,
  Text,
  View,
} from "native-base";
import image from "./image/web_hi_res_512.png"
//import styles from "./styles";
export interface Props {
  loginForm: any;
  onLogin: Function;
}
export interface State {}
class Login extends React.Component<Props, State> {
  render() {
    return (
      <Container>
        <Header style={{ height: 200 }}>
          <Body style={{ alignItems: "center" }}>
          <Image
              style={{
                width: Dimensions.get("window").width * 0.50,
                  height: Dimensions.get("window").width * 0.50}}
              source={image}
          />
            <Title>Telecom Advocates</Title>
            {/*<View padder>*/}
              {/*<Text style={{ color: Platform.OS === "ios" ? "#000" : "#FFF" }}>*/}
                {/**/}
              {/*</Text>*/}
            {/*</View>*/}
          </Body>
        </Header>
        <Content>
          {this.props.loginForm}
          <View padder>
            <Button block onPress={() => this.props.onLogin()}>
              <Text>Login</Text>
            </Button>
          </View>
        </Content>
        {/*<Footer style={{ backgroundColor: "#F8F8F8" }}>*/}
          {/*<View*/}
            {/*style={{ alignItems: "center", opacity: 0.5, flexDirection: "row" }}*/}
          {/*>*/}
            {/*<View padder>*/}
              {/*<Text style={{ color: "#000" }}>*/}
                {/*Made with love at Bai Web and Mobile Lab*/}
              {/*</Text>*/}
            {/*</View>*/}
          {/*</View>*/}
        {/*</Footer>*/}
      </Container>
    );
  }
}

export default Login;
