/**
 * Created by jan on 4/20/18.
 * Last modified by Iva
 */
import * as React from "react";
import { View } from "react-native";
import { Input, Button, Text, Form, Item } from "native-base";
// import { Col, Grid } from "react-native-easy-grid";

class AddTaskComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            task: ""
        }
    }
    render() {
        return (
            <Form>
            <View style={{flexDirection: "row", padding: 10,}}>
                <Item regular style={{width: 200, marginRight: 10}}>
                <Input
                    placeholder="Task"
                    onChangeText={(text) => this.props.onChangeTaskName(text)}
                    value={this.props.taskName}
                    />
                </Item>
                <Button onPress={() => {
                    this.props.taskAdd(this.props.taskName)
                    // console.log(this.state.task)
                }}>
                    <Text>
                        Add Task
                    </Text>
                </Button>
            </View>
            </Form>
        );
    }
}

export default AddTaskComponent;
