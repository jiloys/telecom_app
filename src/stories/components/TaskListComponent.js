import * as React from "react";
import { FlatList, View, TouchableOpacity, Dimensions } from "react-native";
import { Text, Col, Grid, Button } from "native-base";

export default class TaskListComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    // FlatList boys
    _renderItem = ({ item, index }) => {
        return (
            <View style={{width: "100%"}}>
              <Grid>
                  <Col style={{alignItems: "center", justifyContent: "center"}}>
                      <Text>
                          {item.task}
                      </Text>
                  </Col>
                  <Col style={{alignItems: "center", justifyContent: "center"}}>
                      <Text>
                          In
                      </Text>
                      <Text>
                          ({item.in_time})
                      </Text>
                  </Col>
                  <Col style={{alignItems: "center", justifyContent: "center"}}>
                      <Text>
                          Out
                      </Text>
                      <Text>
                          ({item.out_time})
                      </Text>
                  </Col>
                  {item.out_time <= item.in_time ? (
                      <Col style={{alignItems: "center", justifyContent: "center", padding: 5}} >
                      <Button onPress={() => this.props.timeOutTask(item)} style={{alignItems: "center", justifyContent: "center"}}>
                          <Text>
                              Time Out
                          </Text>
                      </Button>
                    </Col>) : null}

              </Grid>
            </View>
        );
    };

    render() {
        return (
            <View>
                {this.props.tasklist ? (<Text style={{alignSelf: "center"}}>Tasks</Text>
                ):null}
                <FlatList
                    data={this.props.taskList}
                    keyExtractor={(item, index) => index}
                    renderItem={this._renderItem}
                    // onEndReachedThreshold={1}
                    // onEndReached={() => this.props.onEndReached()}
                />
            </View>
        );
    }
}
