/**
 * Created by jan on 4/20/18.
 * Last modified by Iva
 */
import * as React from "react";
import { View } from "react-native";
import { Button, Text } from "native-base";
// import { Col, Grid } from "react-native-easy-grid";

class GetTaskButtonComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            task: ""
        }
    }
    render() {
        return (
            <View style={{flexDirection: "row", width: "100%", alignSelf: "center", justifyContent: "center"}}>
                <Button style={{alignSelf: "center", width: 100, alignSelf: "center", justifyContent: "center"}} onPress={() => this.props.getTask()}>
                    <Text>
                        Get Task
                    </Text>
                </Button>
            </View>
        );
    }
}

export default GetTaskButtonComponent;
