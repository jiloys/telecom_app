// @flow
import * as React from "react";
import { observer, inject } from "mobx-react/native";
import { Toast } from "native-base";
import Home from "../../stories/screens/Home";
import { attendanceCheck, addTask, getTask, timeOut } from "../../store/ViewStore/apiLoginCheck";


export interface Props {
  navigation: any;
  mainStore: any;
}
export interface State {}

@inject("loginStore")
@observer
export default class HomeContainer extends React.Component<Props, State> {
  constructor(props){
    super(props);
    this.state = {
      tasks: [],
      latitude: 0,
      longitude: 0,
      taskName: ""
    }
  }
  async componentWillMount (){
    console.log("In")
      await attendanceCheck({
          user_name: this.props.loginStore.user_name,
          password: this.props.loginStore.password,
          date: Date.now()
      })
  }
  async taskAdd(data){
      console.log(data)
      await navigator.geolocation.getCurrentPosition(
          (position) => {
              console.log("wokeeey");
              console.log(position);
              this.setState({
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
              });
          },
          (error) => {
              console.log("AAJDLKAHSDKH")
              console.log(error)
          },

          { enableHighAccuracy: false, timeout: 200000, maximumAge: 3600000 },
      )
      console.log("Oraayt")
    await addTask({
        user_name: this.props.loginStore.user_name,
        password: this.props.loginStore.password,
        date: Date.now(),
        task: data,
        latitude: this.state.latitude,
        longitude: this.state.longitude
    }).then(result => {
        if(result){
            if(result.status === "Success"){
                this.setState({taskName: ""})
                Toast.show({
                    text: "Task successfully added",
                    duration: 2000,
                    position: "bottom",
                    type: "success",
                    textStyle: {textAlign: "center"},
                });
            }
            else if (result.status === "Fail"){
                Toast.show({
                    text: "Task Already Exist",
                    duration: 2000,
                    position: "bottom",
                    type: "danger",
                    textStyle: {textAlign: "center"},
                });
            }
            else if (result.status === "Failed"){
                Toast.show({
                    text: "Adding Task Failed. Please Try Again",
                    duration: 2000,
                    position: "bottom",
                    type: "danger",
                    textStyle: {textAlign: "center"},
                });
            }
        }
    })
  }
    async getTask(){
        await getTask({
            user_name: this.props.loginStore.user_name,
            password: this.props.loginStore.password,
            date: Date.now(),
        }).then(tasks => {
            if(tasks){
                console.log("TASSSSSk")
                console.log(tasks)
                this.setState({tasks: tasks.tasks})
            }
        })
    }
    async timeOutTask(item){

        await navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("wokeeey");
                console.log(position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            (error) => {
                console.log("AAJDLKAHSDKH")
                console.log(error)
            },

            { enableHighAccuracy: false, timeout: 200000 },
        );
        await timeOut({
            user_name: this.props.loginStore.user_name,
            password: this.props.loginStore.password,
            date: Date.now(),
            task: item.task,
            latitude: this.state.latitude,
            longitude: this.state.longitude
        }).then(status => {
            if(status){
                if(status.status === "Success"){
                    this.setState({tasks: status.newObject.tasks})
                    Toast.show({
                        text: "Time Out Successful",
                        duration: 2000,
                        position: "bottom",
                        type: "success",
                        textStyle: {textAlign: "center"},
                    });
                } else if (status.status === "Failed"){
                    this.setState({tasks: status.newObject.tasks})
                    Toast.show({
                        text: "Time Out Failed. Please Try Again",
                        duration: 2000,
                        position: "bottom",
                        type: "danger",
                        textStyle: {textAlign: "center"},
                    });
                }
            }
        })
    }
  render() {

    return <Home
        navigation={this.props.navigation}
        status={this.props.loginStore.status}
        onTimeIn={() => this.onTimeIn()}
        onTimeOut={() => this.onTimeOut()}
        taskAdd={(task) => this.taskAdd(task)}
        getTask={(task) => this.getTask(task)}
        timeOutTask={(item) => this.timeOutTask(item)}
        tasksList={this.state.tasks}
        taskName={this.state.taskName}
        onChangeTaskName={(text) => this.setState({taskName: text})}
    />;
  }
}
