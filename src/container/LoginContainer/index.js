// @flow
import * as React from "react";
import { Item, Input, Icon, Form, Toast } from "native-base";
import { observer, inject } from "mobx-react/native";

import Login from "../../stories/screens/Login";
import { apiLoginCheck } from "../../store/ViewStore/apiLoginCheck";

export interface Props {
  navigation: any;
  loginForm: any;
}
export interface State {}

@inject("loginStore")
@observer
export default class LoginContainer extends React.Component<Props, State> {
  constructor(props){
    super(props);
    this.state = {
      user_name: "",
      password: "",
        date: Date.now(),
    }
  }
  async login() {
    await apiLoginCheck(this.state).then(result => {
        if(result && !result.auth){

                Toast.show({
                    text: result.message,
                    duration: 2000,
                    position: "bottom",
                    type: "danger",
                    textStyle: { textAlign: "center" },
                });
        }
         else {
            this.props.loginStore.changeStatus(result.existing)
            this.props.loginStore.changeUserName(this.state.user_name)
            this.props.loginStore.changePassword(this.state.password)
            this.props.navigation.navigate("Home");
        }
    })

    // this.props.loginForm.validateForm();
    // if (this.props.loginForm.isValid) {
    //   this.props.loginForm.clearStore();
    //   this.props.navigation.navigate("Drawer");
    // } else {
    //   Toast.show({
    //     text: "Enter Valid Email & password!",
    //     duration: 2000,
    //     position: "top",
    //     textStyle: { textAlign: "center" },
    //   });
    // }
  }

  render() {
    const form = this.props.loginForm;
    const Fields = (
      <Form>
        <Item>
          <Icon active name="person" />
          <Input
            placeholder="Email"
            keyboardType="email-address"
            value={this.state.user_name}
            onChangeText={e => this.setState({user_name: e})}
          />
        </Item>
        <Item >
          <Icon active name="unlock" />
          <Input
            placeholder="Password"
            value={this.state.password}
            onChangeText={e => this.setState({password: e})}
            secureTextEntry={true}
          />
        </Item>
      </Form>
    );
    return (
      <Login
        navigation={this.props.navigation}
        loginForm={Fields}
        onLogin={() => this.login()}
      />
    );
  }
}
